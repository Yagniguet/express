//création de modèle

const mongoose = require('mongoose');
const {Schema} = mongoose;

const productSchema = new Schema({
    name : {type: String, default:"", require: true},
    description : String,
    prix : Number,
    quantity : Number
})

const ProductModel = mongoose.model('Product', productSchema)

module.exports = ProductModel; 