var express = require('express');
var router = express.Router();
const data = require ('./data.json')
const ProductModel= require('../models/product')

const array = data;

/* GET home page. */
router.get('/', async function(req, res, next) {
    const data = await ProductModel.find()
    res.status(200).json(data);
});

router.get ('/:id', async function (req, res, next){
    

    const data = await ProductModel.findOne({_id: req.params.id});
    if(data){
        res.status(200).json(data);
    }else{
        res.status(404).json({message: "Data not find"});
    }
});

router.delete('/:id', async function (req, res, next){
    const data = await ProductModel.deleteOne({_id: req.params.id});
    if(data){
        res.status(200).json(data);
    }else{
        res.status(404).json({message: "Data not found"});
    }
});

router.put('/:id', async function  (req, res, next){
    const data = await ProductModel.findByIdAndUpdate(req.params.id, req.body)
    if(data){
       res.status(200).json(data)
    } else{
        res.status(404).json({message: "Data not found"})
    }
});

router.post('/',async function (req ,res ,next){
    const data = await new ProductModel(req.body)
    data.save();
    res.status(200).json(array)
});

module.exports = router;
